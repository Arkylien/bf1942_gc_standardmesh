subshader "rebmine_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1 1 1;
	envmap true;
	texture "texture/reb_av_mine";
}

subshader "rebmine_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "Mods\GCMOD\Movies\reb_av_mine.bik";
}

