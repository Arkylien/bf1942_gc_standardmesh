subshader "BritHead_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.4 0.4 0.4;
	materialSpecular 0 0 0;
	materialSpecularPower 12.5;
	texture "texture/Eye01_r";
}

subshader "BritHead_Material33" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.4 0.4 0.4;
	materialSpecular 0 0 0;
	materialSpecularPower 12.5;
	texture "texture/Mouth1_Z";
}

subshader "BritHead_Material34" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.4 0.4 0.4;
	materialSpecular 0 0 0;
	materialSpecularPower 12.5;
	texture "texture/face_bri1_h";
}

