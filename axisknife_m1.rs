subshader "axisknife_m1_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1 1 1;
	envmap true;
	texture "texture/vib_blade";
}

subshader "axisknife_m1_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1 1 1;
	envmap true;
	texture "texture/vib_handle";
}

