subshader "harpoon_m1_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.65 0.65 0.65;
	materialSpecularPower 12.5;
	envmap true;
	texture "texture/buildings/judicator/jud_doorinside";
}

