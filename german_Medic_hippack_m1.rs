subshader "Jap_Medic_hippack_m1_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.1 0.1 0.1;
	envmap true;
	twosided true;
	texture "texture/snowtrooper/hippac.dds";
}
