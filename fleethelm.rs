subshader "fleethelm_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialSpecular 1 1 1;
	materialSpecularPower 40;
	twosided true;
	envmap true;
	transparent false;
	texture "texture/fleet_helmet";
}

