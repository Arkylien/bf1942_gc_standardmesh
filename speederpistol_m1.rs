subshader "speederpistol_m1_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1 1 1;
	materialSpecular 0.650 0.650 0.650;
	materialSpecularPower 12.5;
	envmap true;
	texture "texture/Weapons/speederpistol";
}
