subshader "russhelm_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.5 0.5 0.5;
	materialSpecular 1 1 1;
	twosided true;
	envmap true;
	texture "texture/tat-rebel_helmet";
}

