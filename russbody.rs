subshader "russbody_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	materialAmbient 1 1 1;
	texture "texture/tat-rebel";
}
subshader "russbody_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/tat-rsoul";
}
