subshader "Small_Bomb_M1_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	lightingSpecular true;
	materialSpecular 0.9 0.9 0.9;
	materialSpecularPower 5;
	transparent true;
	texture "texture/Weapons/tiebomb";
}
