subshader "Grass2_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	transparent true;
	blendSrc sourceAlpha;
	twosided true;
	texture "texture/grass2_1";
}
subshader "Grass2_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	transparent true;
	blendSrc sourceAlpha;
	twosided true;
	texture "texture/grass2_3";
}
subshader "Grass2_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	transparent true;
	blendSrc sourceAlpha;
	twosided true;
	texture "texture/grass2_2";
}
