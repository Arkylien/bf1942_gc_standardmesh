subshader "E-11_Plupp_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1 1 1;
	materialSpecular 0.650 0.650 0.650;
	materialSpecularPower 12.5;
	envmap true;
	texture "texture/Weapons/e11-powerpack";
}
