subshader "Shad_E-11_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Weapons/e11-scope";
}
subshader "Shad_E-11_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Weapons/e11-sidepart";
}
subshader "Shad_E-11_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Weapons/e11-handle";
}
subshader "Shad_E-11_Material3" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Weapons/e11-sidepart";
}
subshader "Shad_E-11_Material4" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Weapons/e11-bigsidepart";
}
subshader "Shad_E-11_Material5" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Weapons/e11-powerpack";
}
subshader "Shad_E-11_Material6" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Weapons/e11-trigger";
}
subshader "Shad_E-11_Material7" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Weapons/E11-barrel";
}
