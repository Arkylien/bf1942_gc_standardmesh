subshader "Japhead_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.5 0.5 0.5;
	materialSpecular 0.25 0.25 0.25;
	envmap true;
	texture "texture/snowtrooper/head.dds";
}
