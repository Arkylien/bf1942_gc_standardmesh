subshader "monument_Material0" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/buildings/dantooine/dant_rock";
}

subshader "monument_Material1" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
        transparent true;
        depthWrite false;
	texture "texture/mon_1";
}

subshader "monument_Material2" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/mon_4";
}

subshader "monument_Material3" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/mon_1na";
}

subshader "monument_Material4" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/mon_3pic";
}

subshader "monument_Material5" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/mon_6";
}

subshader "monument_Material6" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/mon_2";
}

subshader "monument_Material7" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Sky_hoth_01";
}

subshader "monument_Material8" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.488235 0.488235 0.488235;
	texture "texture/Sky_hoth_01";
}

