subshader "Sky_Bocage_M1_Material0" "StandardMesh/Default"
{
	lighting false;
	texture "texture/Sky_hothrr_05";
}

subshader "Sky_Bocage_M1_Material1" "StandardMesh/Default"
{
	lighting false;
	texture "texture/Sky_hothrr_06";
}

subshader "Sky_Bocage_M1_Material2" "StandardMesh/Default"
{
	lighting false;
	texture "texture/Sky_hothrr_01";
}

subshader "Sky_Bocage_M1_Material3" "StandardMesh/Default"
{
	lighting false;
	texture "texture/Sky_hothrr_02";
}

subshader "Sky_Bocage_M1_Material4" "StandardMesh/Default"
{
	lighting false;
	texture "texture/Sky_hothrr_03";
}

subshader "Sky_Bocage_M1_Material5" "StandardMesh/Default"
{
	lighting false;
	texture "texture/Sky_hothrr_04";
}

