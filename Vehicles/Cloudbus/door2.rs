subshader "door2_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 4.0;
	envmap true;
	twosided true;
	texture "texture/Vehicles/Cloudbus/cloudbus1_v2";
}
