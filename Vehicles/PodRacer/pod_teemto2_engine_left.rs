subshader "pod_teemto2_engine_left_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/PodRacer/pod_teemto2_engine_id1";
}

subshader "pod_teemto2_engine_left_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/PodRacer/pod_teemto_engine_id1";
}

