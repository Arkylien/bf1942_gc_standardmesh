subshader "atatde_body_m1_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.584314 0.584314 0.584314;
	texture "texture/vehicles/atat/atat_main_mat_id_1";
}

subshader "atatde_body_m1_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.584314 0.584314 0.584314;
	texture "texture/vehicles/atat/atat_interior_mat_id_4";
}

subshader "atatde_body_m1_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.584314 0.584314 0.584314;
	texture "texture/vehicles/atat/atat_interior_mat_id_2";
}

subshader "atatde_body_m1_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.584314 0.584314 0.584314;
	texture "texture/vehicles/atat/floor";
}

