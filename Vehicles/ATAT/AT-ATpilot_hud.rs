subshader "AT-ATpilot_hud_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
        transparent true;
        depthWrite false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/ATAT/AT-ATpilot_hud";
}

