subshader "ATPTTexHead5_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/atptdsplain";
}

subshader "ATPTTexHead5_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/4waytest";
}

subshader "ATPTTexHead5_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/lambda_int";
}

subshader "ATPTTexHead5_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/lambda_int";
}

subshader "ATPTTexHead5_Material4" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/lambda_int";
}

subshader "ATPTTexHead5_Material5" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/4waytest";
}

subshader "ATPTTexHead5_Material6" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/4waytest";
}

subshader "ATPTTexHead5_Material7" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/4waytest";
}

subshader "ATPTTexHead5_Material8" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/lambda_int";
}

subshader "ATPTTexHead5_Material9" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/lambda_win";
}

subshader "ATPTTexHead5_Material10" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/lambda_win";
}

subshader "ATPTTexHead5_Material11" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/4waytest";
}

subshader "ATPTTexHead5_Material12" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/4waytest";
}

subshader "ATPTTexHead5_Material13" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/4waytest";
}

subshader "ATPTTexHead5_Material14" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/lambda_win";
}

subshader "ATPTTexHead7_Material15" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/atptdscircles";
}

subshader "ATPTTexHead5_Material17" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/atptdsplain";
}

subshader "ATPTTexHead5_Material18" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/atptdsplain";
}

subshader "ATPTTexHead5_Material19" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/4waytest";
}

subshader "ATPTTexHead5_Material20" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/4waytest";
}

subshader "ATPTTexHead5_Material21" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/4waytest";
}

subshader "ATPTTexHead5_Material22" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/4waytest";
}

subshader "ATPTTexHead7_Material22" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/atptdsplain";
}

subshader "ATPTTexHead7_Material23" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/atptdscircles";
}

subshader "ATPTTexHead5_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/ATPT/ATPTLeftDetail";
}

