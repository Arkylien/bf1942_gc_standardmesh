subshader "imp_skiff_door_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1 1 1;
	materialSpecular 0.470588 0.450980 0.627450;
	materialSpecularPower 40;
	transparent false;
	twosided false;
	envmap true;
	depthwrite true;
	texture "texture/Vehicles/impskiff/impskiff";
}

