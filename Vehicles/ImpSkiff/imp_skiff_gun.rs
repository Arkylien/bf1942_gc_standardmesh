subshader "imp_skiff_gun_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.470588 0.45098 0.62745;
	materialSpecularPower 40.0;
	envmap true;
	depthwrite true;
	texture "texture/Vehicles/impskiff/impskiff";
}

