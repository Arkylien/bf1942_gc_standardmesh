subshader "powerdroid_body_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Vehicles/Powerdroid/powerdroid-body";
}
subshader "powerdroid_body_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Vehicles/Powerdroid/powerdroid-leg";
}
