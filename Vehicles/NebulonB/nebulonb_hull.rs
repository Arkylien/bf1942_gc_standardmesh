subshader "nebulonb_hull_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/nebulonb/tex1";
}

subshader "nebulonb_hull_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/nebulonb/tex7";
}

subshader "nebulonb_hull_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/buildings/tanaab/tan_isd04";
}

subshader "nebulonb_hull_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/buildings/tanaab/tan_isd05";
}

subshader "nebulonb_hull_Material4" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/vehicles/nebulonb/tex1";
}

subshader "nebulonb_hull_Material5" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/nebulonb/tex5";
}

subshader "nebulonb_hull_Material6" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/nebulonb/tex4";
}

subshader "nebulonb_hull_Material7" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/nebulonb/tex3";
}

subshader "nebulonb_hull_Material8" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/nebulonb/tex2";
}

subshader "nebulonb_hull_Material9" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/nebulonb/rstripe";
}

subshader "nebulonb_hull_Material10" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/nebulonb/tex3";
}

subshader "nebulonb_hull_Material11" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/buildings/tanaab/tan_frig04";
}

subshader "nebulonb_hull_Material12" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	twosided true;
	envmap true;
	texture "texture/vehicles/nebulonb/tex8";
}

