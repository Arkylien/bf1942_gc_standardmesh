subshader "nebulonb_inside_Material0" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 0.588235 0.588235 0.588235;
	envmap true;
	lightingSpecular true;
	texture "texture/buildings/superecho/echo_lights";
}

subshader "nebulonb_inside_Material1" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 0.588235 0.588235 0.588235;
	envmap true;
	lightingSpecular true;
	texture "texture/buildings/superecho/controlrooflight";
}

subshader "nebulonb_inside_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	envmap true;
	lightingSpecular true;
	texture "texture/buildings/superecho/control_panel_A";
}

subshader "nebulonb_inside_Material3" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	envmap false;
	lightingSpecular true;
	materialSpecular 0.588235 0.588235 0.588235;
	texture "texture/buildings/superecho/control_floor";
}

subshader "nebulonb_inside_Material4" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	envmap true;
	lightingSpecular true;
	texture "texture/buildings/superecho/miniscreenA";
}

subshader "nebulonb_inside_Material5" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	envmap false;
	lightingSpecular true;
	materialSpecular 0.588235 0.588235 0.588235;
	texture "texture/buildings/superecho/wallpanel";
}

