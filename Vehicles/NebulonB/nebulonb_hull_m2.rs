subshader "nebulonb_hull_m2_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.1 0.1 0.1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	envmap false;
	texture "texture/Vehicles/nebulonb/tex1";
}
subshader "nebulonb_hull_m2_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.1 0.1 0.1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	envmap false;
	texture "texture/Vehicles/nebulonb/tex7";
}
subshader "nebulonb_hull_m2_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.1 0.1 0.1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	envmap false;
	texture "texture/Vehicles/nebulonb/tex6";
}
subshader "nebulonb_hull_m2_Material3" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 0.1 0.1 0.1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	envmap false;
	texture "texture/Vehicles/nebulonb/tex5";
}
subshader "nebulonb_hull_m2_Material4" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.1 0.1 0.1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	envmap false;
	texture "texture/Vehicles/nebulonb/tex4";
}
subshader "nebulonb_hull_m2_Material5" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.1 0.1 0.1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	envmap false;
	texture "texture/Vehicles/nebulonb/tex3";
}
subshader "nebulonb_hull_m2_Material6" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.1 0.1 0.1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	envmap false;
	texture "texture/Vehicles/nebulonb/tex2";
}
subshader "nebulonb_hull_m2_Material7" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.1 0.1 0.1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	envmap false;
	texture "texture/vehicles/nebulonb/rstripe";
}
subshader "nebulonb_hull_m2_Material8" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 0.1 0.1 0.1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	envmap false;
	texture "texture/buildings/tanaab/tan_frig04";
}
subshader "nebulonb_hull_m2_Material9" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.1 0.1 0.1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	envmap false;
	texture "texture/buildings/tanaab/tan_isd04";
}
subshader "nebulonb_hull_m2_Material10" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 0.1 0.1 0.1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	envmap false;
	texture "texture/buildings/tanaab/tan_isd05";
}

