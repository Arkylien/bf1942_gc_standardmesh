subshader "amph_tur_x_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialSpecular 0.5 0.5 0.5;
	materialDiffuse 0.588235 0.588235 0.588235;
	envmap true;
	texture "texture/vehicles/amph/amphtex";
}

subshader "amph_tur_x_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialSpecular 0.5 0.5 0.5;
	materialDiffuse 0.588 0.588 0.588;
	envmap true;
	texture "texture/vehicles/amph/floor";
}

