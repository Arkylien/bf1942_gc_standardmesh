subshader "slave1_low_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.588235 0.588235 0.588235;
	materialSpecular 0.9 0.9 0.9;
	materialSpecularPower 27.0;
	texture "texture/Vehicles/Slave1/slave1_hull";
}

subshader "slave1_low_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.588235 0.588235 0.588235;
	materialSpecular 0.9 0.9 0.9;
	materialSpecularPower 85.0;
	texture "texture/Vehicles/Slave1/slave1_hull";
}

