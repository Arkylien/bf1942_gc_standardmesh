subshader "R2D2x_wreck_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/Vehicles/R2/r2wreck";
}

subshader "R2D2x_wreck_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/Vehicles/R2/r2wreck";
}

