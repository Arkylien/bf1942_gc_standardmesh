subshader "r2d2x_head_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/Vehicles/R2/r2";
}

subshader "r2d2x_head_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "mods/GCMOD/movies/R2D2x";
}

