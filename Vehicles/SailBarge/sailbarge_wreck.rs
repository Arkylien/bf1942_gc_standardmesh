subshader "sailbarge_wreck_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/Vehicles/SailBarge/sailbarge_wreck";
}

subshader "sailbarge_interior_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/Vehicles/SailBarge/sailbarge_interior";
}

subshader "sailbarge_wreck_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/SailBarge/sailbarge_flap";
}

