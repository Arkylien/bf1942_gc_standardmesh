subshader "sailbarge_hull_door_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/Vehicles/SailBarge/sailbarge_hull";
}

subshader "sailbarge_hull_door_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/SailBarge/sailbarge_interior";
}

