subshader "sailbarge_static_Material0" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	twosided true;
	texture "texture/Vehicles/SailBarge/sailbarge_hull_low";
}

subshader "sailbarge_static_Material1" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular true;
	materialDiffuse 0.588235 0.588235 0.588235;
	materialSpecular 0.9 0.9 0.9;
	materialSpecularPower 22.0;
	transparent true;
	twosided true;
	texture "texture/Vehicles/SailBarge/sailbarge_sails";
}

subshader "sailbarge_static_Material2" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/SailBarge/sailbarge_flap";
}

subshader "sailbarge_static_Material3" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Vehicles/SailBarge/sailbarge_flap";
}

subshader "sailbarge_static_Material4" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/Vehicles/SailBarge/sailbarge_hull_low";
}

subshader "sailbarge_static_Material5" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/Vehicles/SailBarge/sailbarge_interior";
}

subshader "sailbarge_static_Material6" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/SailBarge/sailbarge_flap";
}

