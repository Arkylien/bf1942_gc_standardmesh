subshader "sailbarge_sails_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	lightingSpecular true;
	materialSpecular 0.9 0.9 0.9;
	materialSpecularPower 22.0;
	twosided true;
        transparent true;
	texture "texture/Vehicles/SailBarge/sailbarge_sails";
}
