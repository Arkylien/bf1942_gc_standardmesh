subshader "spacetur_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
        materialSpecular 0.8 0.8 1.0;
	materialSpecularPower 4.0;
	envmap true;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/vehicles/I3satellite/spt";
}
