subshader "carrack_hull_Material0" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
        twosided true;
	texture "texture/vehicles/carrack/body";
}

subshader "carrack_hull_Material1" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/carrack/body2";
}

subshader "carrack_hull_Material2" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/carrack/body3";
}

subshader "carrack_hull_Material3" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/carrack/body4";
}

subshader "carrack_hull_Material4" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/carrack/body5";
}

subshader "carrack_hull_Material5" "StandardMesh/Default"
{
	
	lighting true;
	envmap false;
	materialDiffuse 1.0 1.0 1.0;
	envmap true;
	texture "texture/vehicles/carrack/body6";
}

subshader "carrack_hull_Material6" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
      	transparent true;
      	texture "texture/vehicles/carrack/window";
}

