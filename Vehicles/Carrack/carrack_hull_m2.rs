subshader "carrack_hull_m2_Material0" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	twosided true;
	texture "texture/vehicles/carrack/body";
}
subshader "carrack_hull_m2_Material1" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/carrack/body2";
}
subshader "carrack_hull_m2_Material2" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/carrack/body3";
}
subshader "carrack_hull_m2_Material3" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/carrack/body4";
}
subshader "carrack_hull_m2_Material4" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/carrack/body5";
}
subshader "carrack_hull_m2_Material5" "StandardMesh/Default"
{
	lighting false;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/carrack/body6";
}
subshader "carrack_hull_m2_Material6" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Black_L";
}
subshader "carrack_hull_m2_Material7" "StandardMesh/Default"
{
	lighting true;
	envmap false;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 4.0;
	materialDiffuse 0.1 0.1 0.1;
	texture "texture/vehicles/carrack/wall";
}
subshader "carrack_hull_m2_Material8" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/carrack/body3";
}
subshader "carrack_hull_m2_Material9" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/carrack/floor2";
}
