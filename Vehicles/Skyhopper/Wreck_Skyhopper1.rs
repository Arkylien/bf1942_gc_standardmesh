subshader "Wreck_Skyhopper1_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 4.0;
	twosided true;
	envmap true;
	texture "texture/Vehicles/Skyhopper/Wreck_Skyhopper";
}
