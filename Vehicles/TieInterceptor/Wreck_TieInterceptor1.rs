subshader "Wreck_TieInterceptor1_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	twosided true;
	envmap true;
	texture "texture/Vehicles/TieInterceptor/Wreck_TieInterceptor";
}
