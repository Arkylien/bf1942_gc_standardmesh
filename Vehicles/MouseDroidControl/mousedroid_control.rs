subshader "mousedroid_control_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/SuperEcho/control_floor";
}

subshader "mousedroid_control_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/SuperEcho/wallgrate";
}

subshader "mousedroid_control_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "mods/gcmod/movies/pro2.bik";
}

subshader "mousedroid_control_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "mods/gcmod/movies/New_Pro.bik";
}

subshader "mousedroid_control_Material4" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/SuperEcho/controlrooflight";
}

subshader "mousedroid_control_Material5" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/SuperEcho/control_panel_A";
}

