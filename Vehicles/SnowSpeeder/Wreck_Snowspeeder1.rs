subshader "Wreck_Snowspeeder1_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	twosided true;
	envmap true;
	texture "texture/Vehicles/Snowspeeder/Wreck_Snowspeeder";
}
