subshader "ATSTxHipsVC_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.2 0.2 0.2;
	materialSpecularPower 5.0;
	transparent true;
	texture "texture/vehicles/atstx/ATSTxHipsVC";
}

