subshader "ATSTx_Gunner_Cockpit_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	transparent true;
        depthwrite false; 
	texture "texture/Vehicles/ATST/hudskin";
}

subshader "ATSTx_Gunner_Cockpit_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "mods/GCMOD/movies/ATST_cockpit2";
}


subshader "ATSTx_Gunner_Cockpit_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "mods/GCMOD/movies/ATST_cockpit1";
}

