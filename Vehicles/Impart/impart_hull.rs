subshader "impart_hull_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.588 0.588 0.588;
	materialSpecular 0.5 0.5 0.5;
	materialSpecularPower 1.0;
	envmap true;
	texture "texture/Vehicles/Impart/impart_hull";
}

