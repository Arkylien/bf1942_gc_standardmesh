subshader "Cloudcar_Flap_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.2 0.2 0.2;
	materialSpecularPower 1.0;
	twosided false;
        transparent true;
	texture "texture/Vehicles/Cloudcar/cloudcar_flap";
}
