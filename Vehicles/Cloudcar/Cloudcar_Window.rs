subshader "Cloudcar_Window_Material0" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
        transparent true;
	depthWrite true;
	twosided true;
	texture "texture/Vehicles/Cloudcar/cloudcar_window";
}
