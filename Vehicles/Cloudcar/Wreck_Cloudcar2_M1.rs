subshader "Wreck_Cloudcar2_M1_Material0" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.2 0.2 0.2;
	materialSpecularPower 1.0;
	texture "texture/Vehicles/Cloudcar/Wreck_Cloudcar";
}

