subshader "rocket_Material0" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	transparent true;
	twosided true;
	texture "texture/vehicles/rocket_eng";
}

subshader "rocket_Material1" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	transparent true;
	twosided true;
	texture "texture/vehicles/rocket_eng";
}

subshader "rocket_Material2" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	transparent true;
	twosided true;
	texture "texture/vehicles/rocket_eng";
}

