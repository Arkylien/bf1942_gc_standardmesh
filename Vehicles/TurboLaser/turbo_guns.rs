subshader "turbo_guns_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.8 0.8 0.8;
	lightingSpecular true;
	materialSpecular 0.5 0.5 0.5;
	materialSpecularPower 1.0;
	envmap true;
	texture "texture/Vehicles/TurboLaser/turbolaser_base";
}
