subshader "imp_escapepod_Material0" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/impescpod/imp_escapepod_id1";
}

subshader "imp_escapepod_Material1" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/impescpod/imp_escapepod_id2";
}

