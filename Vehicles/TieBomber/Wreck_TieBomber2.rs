subshader "Wreck_TieBomber2_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	twosided true;
	envmap true;
	texture "texture/Vehicles/TieBomber/Wreck_TieBomber";
}
