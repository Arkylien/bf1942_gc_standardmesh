subshader "awing_cockpit_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	lightingSpecular true;
	materialSpecular 0.5 0.5 0.5;
	materialSpecularPower 1.0;
        twosided true;
	envmap true;
	texture "texture/Vehicles/AWing/awing_cockpit";
}
