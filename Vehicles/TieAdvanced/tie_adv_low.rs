subshader "tie_adv_low_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Vehicles/TieAdvanced/tie_advanced";
}

subshader "tie_adv_low_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	envmap true;
	texture "texture/Vehicles/TieAdvanced/tie_advanced_win";
}

