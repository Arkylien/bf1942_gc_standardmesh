subshader "Wreck_APTurret2_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/APTurret/turretbarrel_wreck";
}

subshader "Wreck_APTurret2_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/APTurret/Wreck_APTurret2";
}

