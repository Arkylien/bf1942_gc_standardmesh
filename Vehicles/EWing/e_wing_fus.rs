subshader "e_wing_fus_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/vehicles/ewing/ewing_id1";
}

subshader "e_wing_fus_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
      materialDiffuse 0.909804 0.901961 0.91;
	texture "texture/vehicles/ewing/ewing_id2";
}

