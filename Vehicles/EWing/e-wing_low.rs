subshader "e-wing_low_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/vehicles/ewing/ewing_id1";
}

subshader "e-wing_low_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.909804 0.901961 0.91;
	texture "texture/vehicles/ewing/ewing_id2";
}

subshader "e-wing_low_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/vehicles/ewing/ewing_id1";
}

subshader "e-wing_low_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/vehicles/ewing/ewing_id1";
}

