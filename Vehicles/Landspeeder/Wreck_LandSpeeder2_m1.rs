subshader "Wreck_LandSpeeder2_m1_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	twosided true;
	envmap true;
	texture "texture/Vehicles/Landspeeder/LandSpeeder_Wreck";
}
