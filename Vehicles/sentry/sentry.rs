subshader "sentry_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.588235 0.588235 0.588235;
	materialSpecular 0.9 0.9 0.9;
	materialSpecularPower 62.0;
	texture "texture/vehicles/sentry/sentrytex";
}

