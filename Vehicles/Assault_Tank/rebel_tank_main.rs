subshader "rebel_tank_main_Material0" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/Assault_Tank/rebtank_hull_id1";
}

subshader "rebel_tank_main_Material1" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/Assault_Tank/rebtank_hull_id2";
}

subshader "rebel_tank_main_Material2" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/Assault_Tank/rebtank_hull_id3";
}

