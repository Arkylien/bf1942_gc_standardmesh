subshader "Rebel_Tank_barrel_pivot_Material0" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Assault_Tank/rebtank_misc_id1";
}

subshader "Rebel_Tank_barrel_pivot_Material1" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Assault_Tank/rebtank_misc_id2";
}

