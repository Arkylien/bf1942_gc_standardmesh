subshader "wreck_rebel_tank_Material0" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/Assault_Tank/wreck_rebtank_hull_id1";
}

subshader "wreck_rebel_tank_Material1" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/Assault_Tank/wreck_rebtank_hull_id2";
}

subshader "wreck_rebel_tank_Material2" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/vehicles/Assault_Tank/wreck_rebtank_hull_id3";
}

