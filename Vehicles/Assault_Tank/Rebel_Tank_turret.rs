subshader "Rebel_Tank_turret_Material0" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Assault_Tank/rebtank_hull_id1";
}

subshader "Rebel_Tank_turret_Material1" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Assault_Tank/rebtank_hull_id3";
}

