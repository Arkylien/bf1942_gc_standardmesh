subshader "gunship_pilot_cockpit_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 4.0;
	envmap true;
	texture "texture/vehicles/repgun/gunship_cockpit_id1";
      	twosided true;
}

subshader "gunship_pilot_cockpit_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 4.0;
	envmap true;
	transparent true;
      	twosided true;
      	depthwrite false;
	texture "texture/vehicles/amph/window";
}

subshader "gunship_pilot_cockpit_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 4.0;
	envmap true;
	texture "texture/vehicles/repgun/gunship_cockpit_id3";
      	twosided true;
}

subshader "gunship_pilot_cockpit_Material3" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 4.0;
	envmap true;
	texture "texture/vehicles/repgun/gunship_cockpit_id4";
      	twosided true;
}

subshader "gunship_pilot_cockpit_Material4" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 4.0;
	envmap true;
	texture "texture/vehicles/repgun/gunship_cockpit_id5";
      	twosided true;
}

subshader "gunship_pilot_cockpit_Material5" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 4.0;
	envmap true;
	texture "texture/vehicles/repgun/gunship_cockpit_id6";
      	twosided true;
}

