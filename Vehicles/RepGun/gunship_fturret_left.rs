subshader "gunship_fturret_left_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 4.0;
	envmap true;
	twosided true;
	texture "texture/vehicles/repgun/gunship_cockpit_id1";
}

