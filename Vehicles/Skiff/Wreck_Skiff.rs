subshader "Wreck_Skiff_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Vehicles/Skiff/Wreck_Skiff";
}
subshader "Wreck_Skiff_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Vehicles/Skiff/Wreck_Skiff2";
}
