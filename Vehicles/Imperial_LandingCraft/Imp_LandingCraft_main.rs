subshader "Imp_LandingCraft_main_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.501961 0.501961 0.501961;
	texture "texture/Vehicles/Imperial_LandingCraft/landing_craft1";
}

subshader "Imp_LandingCraft_main_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.501961 0.501961 0.501961;
	texture "texture/Vehicles/Imperial_LandingCraft/landing_craft_ceil";
}

subshader "Imp_LandingCraft_main_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.584314 0.584314 0.584314;
	texture "texture/Vehicles/Imperial_LandingCraft/landing_craft_wall";
}

subshader "Imp_LandingCraft_main_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.584314 0.584314 0.584314;
	texture "texture/Vehicles/Imperial_LandingCraft/landing_craft_flr";
}

subshader "Imp_LandingCraft_main_Material4" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.584314 0.584314 0.584314;
	texture "texture/Vehicles/Imperial_LandingCraft/landing_craft_ceil";
}

subshader "Imp_LandingCraft_main_Material5" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.584314 0.584314 0.584314;
	texture "texture/Vehicles/Imperial_LandingCraft/landing_craft1";
}

subshader "Imp_LandingCraft_main_Material6" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.584314 0.584314 0.584314;
	transparent true;
	texture "texture/Vehicles/Imperial_LandingCraft/landing_craft1";
}

subshader "Imp_LandingCraft_main_Material7" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.133333 0.133333 0.133333;
	materialSpecular 0.898039 0.898039 0.898039;
	materialSpecularPower 100.0;
	transparent true;
	texture "texture/Vehicles/XWing/xwing_glass";
}

