subshader "TieFighter_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.75 0.75 0.75;
	lightingSpecular true;
	materialSpecular 0.5 0.5 0.5;
	materialSpecularPower 1.0;
	envmap true;
	texture "texture/Vehicles/TieFighter/tiefinal";
}
