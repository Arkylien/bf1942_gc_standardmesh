subshader "landspeeder2_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	envmap true;
	texture "texture/Vehicles/Landspeeder_X/blue";
}

subshader "landspeeder2_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	envmap true;
	texture "texture/Vehicles/Landspeeder_X/cockpit";
}

