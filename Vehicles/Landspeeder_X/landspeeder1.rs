subshader "landspeeder1_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	envmap true;
	texture "texture/Vehicles/Landspeeder_X/red";
}

subshader "landspeeder1_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	envmap true;
	texture "texture/Vehicles/Landspeeder_X/cockpit";
}

