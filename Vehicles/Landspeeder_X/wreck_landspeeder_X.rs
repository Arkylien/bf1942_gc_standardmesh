subshader "wreck_landspeeder_X_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	twosided true;
	envmap true;
	texture "texture/Vehicles/Landspeeder_X/wreck_gain";
}

subshader "wreck_landspeeder_X_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	twosided true;
	envmap true;
	texture "texture/Vehicles/Landspeeder_X/wreck_cockpit";
}

