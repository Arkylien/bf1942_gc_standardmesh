subshader "landspeeder3_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	envmap true;
	texture "texture/Vehicles/Landspeeder_X/green";
}

subshader "landspeeder3_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	envmap true;
	texture "texture/Vehicles/Landspeeder_X/cockpit";
}

