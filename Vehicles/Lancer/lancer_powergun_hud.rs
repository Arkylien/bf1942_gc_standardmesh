subshader "lancer_powergun_hud_Material0" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
        transparent true;
        depthWrite false;
	texture "texture/vehicles/lancer/lancer_powergun_hud";
}

