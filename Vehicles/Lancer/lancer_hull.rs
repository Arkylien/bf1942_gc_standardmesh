subshader "lancer_hull_Material0" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Lancer/Lancertopside_wip18";
}

subshader "lancer_hull_Material1" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Lancer/Lancertopside_wip19";
}

subshader "lancer_hull_Material2" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	twosided true;
	texture "texture/Vehicles/Lancer/Lancertopside_wip20";
}

subshader "lancer_hull_Material3" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Lancer/Lancertopside_wip21";
}

subshader "lancer_hull_Material4" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Lancer/Lancertopside_wip22";
}

subshader "lancer_hull_Material5" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Lancer/Lancertopside_wip32";
}

subshader "lancer_hull_Material6" "StandardMesh/Default"
{
	lighting false;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Lancer/Lancertopside_wip28";
}

subshader "lancer_hull_Material7" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Lancer/Lancertopside_wip25";
}

