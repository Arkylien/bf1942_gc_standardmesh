subshader "lambda_body_Material0" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Lambda/lambda";
}

subshader "lambda_body_Material1" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Lambda/lambda_int";
}

subshader "lambda_body_Material2" "StandardMesh/Default"
{
	lighting true;
	envmap true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Lambda/lambda_win";
}

