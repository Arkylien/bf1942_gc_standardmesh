subshader "Wreck_XWing1_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Vehicles/XWing/Wreck_XWing_Wing";
}
subshader "Wreck_XWing1_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Vehicles/XWing/Wreck_XWing_Fus";
}
