subshader "snowspeeder_cockpit_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Snowspeeder/Cockpit/middletex";
}

subshader "snowspeeder_cockpit_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Snowspeeder/Cockpit/bottomtex";
}

subshader "snowspeeder_cockpit_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	transparent true;
	texture "texture/Vehicles/Snowspeeder/Cockpit/windows";
}

subshader "snowspeeder_cockpit_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Snowspeeder/Cockpit/toptex";
}

subshader "snowspeeder_cockpit_Material4" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Vehicles/Snowspeeder/Cockpit/detailtex";
}

