subshader "runner_dock_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 96.0;
	envmap false;
	texture "texture/vehicles/runner/tan_runner02";
}

