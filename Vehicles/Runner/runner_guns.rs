subshader "runner_guns_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 96.0;
	envmap false;
	texture "texture/vehicles/runner/mainhull";
}

subshader "runner_guns_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 96.0;
	envmap false;
	texture "texture/vehicles/runner/redstripe";
}

subshader "runner_guns_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 96.0;
	envmap false;
	texture "texture/vehicles/runner/tan_runner01";
}

