subshader "runner_gunbase_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 16.0;
	envmap false;
	texture "texture/vehicles/runner/mainhull";
}

subshader "runner_gunbase_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 16.0;
	envmap false;
	texture "texture/vehicles/runner/redstripe";
}

subshader "runner_gunbase_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 16.0;
	envmap false;
	texture "texture/vehicles/runner/tan_runner01";
}

