subshader "brunner_powergun_hud_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
        transparent true;
        depthWrite false;
	texture "texture/vehicles/runner/br_powergun_hud";
}

