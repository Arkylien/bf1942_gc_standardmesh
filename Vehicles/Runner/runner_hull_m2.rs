subshader "runner_hull_m2_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/runner/mainhull";
}

subshader "runner_hull_m2_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/runner/redstripe";
}

subshader "runner_hull_m2_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/runner/junky";
}

subshader "runner_hull_m2_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/runner/engines";
}

subshader "runner_hull_m2_Material4" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/runner/engine3";
}

subshader "runner_hull_m2_Material5" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/runner/mainhull";
}

subshader "runner_hull_m2_Material6" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/runner/redstripe";
}

subshader "runner_hull_m2_Material7" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/runner/tan_runner01";
}

