subshader "runner_hull_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/runner/mainhull";
}

subshader "runner_hull_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/runner/redstripe";
}

subshader "runner_hull_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/runner/junky";
}

subshader "runner_hull_Material3" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/runner/engines";
}

subshader "runner_hull_Material4" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/runner/engine2";
}

subshader "runner_hull_Material5" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/runner/engine3";
}

subshader "runner_hull_Material6" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/vehicles/runner/engine4";
}