subshader "runner_pods_int_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.588235 0.588235 0.588235;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 4.0;
	depthWrite true;
	transparent true;
	envmap true;
	texture "texture/vehicles/runner/podwall2";
}

