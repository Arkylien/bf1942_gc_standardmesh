subshader "spATPTCabin_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/vehicles/ATPT/WIP14";
}

subshader "spATPTCabin_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/vehicles/ATPT/lambda_int";
}

subshader "spATPTCabin_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/vehicles/ATPT/lambda_int";
}

subshader "spATPTCabin_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/vehicles/ATPT/lambda_int";
}

subshader "spATPTCabin_Material4" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/vehicles/ATPT/lambda_win";
}

subshader "spATPTCabin_Material5" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/vehicles/ATPT/lambda_int";
}

subshader "spATPTCabin_Material6" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/vehicles/ATPT/WIP14";
}

