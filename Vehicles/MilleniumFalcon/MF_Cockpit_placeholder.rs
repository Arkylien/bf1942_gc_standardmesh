subshader "MF_Cockpit_placeholder_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.12549 0.12549 0.12549;
	materialSpecularPower 12.5;
	texture "texture/vehicles/MilleniumFalcon/mf_cockpit_placeholder";
}

subshader "MF_Cockpit_placeholder_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.12549 0.12549 0.12549;
	materialSpecularPower 12.5;
	texture "texture/vehicles/MilleniumFalcon/mf_cockpit_placeholder";
}



