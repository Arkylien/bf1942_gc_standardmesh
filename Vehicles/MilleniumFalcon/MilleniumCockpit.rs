subshader "MilleniumCockpit_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.12549 0.12549 0.12549;
	materialSpecularPower 12.5;
	texture "texture/Vehicles/MilleniumFalcon/Image1";
}

subshader "MilleniumCockpit_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.12549 0.12549 0.12549;
	materialSpecularPower 12.5;
	texture "texture/Vehicles/MilleniumFalcon/mf_cockpit";
}

subshader "MilleniumCockpit_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.12549 0.12549 0.12549;
	materialSpecularPower 12.5;
	texture "texture/Vehicles/MilleniumFalcon/mf_chairs";
}