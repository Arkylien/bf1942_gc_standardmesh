subshader "waveskim_hull_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/vehicles/waveskim/Skimmer_Hull2";
}

subshader "waveskim_hull_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/vehicles/waveskim/black";
}

subshader "waveskim_hull_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/vehicles/waveskim/Skimmer_Hull2";
}

