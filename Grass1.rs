subshader "Grass1_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	transparent true;
	blendSrc sourceAlpha;
	twosided true;
	texture "texture/grass1";
}
