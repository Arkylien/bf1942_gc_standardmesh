subshader "lethal_injection_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1 1 1;
	envmap true;
	texture "texture/syringe";
}

subshader "lethal_injection_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1 1 1;
	envmap true;
	texture "texture/capsule";
}

