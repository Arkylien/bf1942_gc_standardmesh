subshader "shield_down_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.611765 0.592157 0.996079;
	transparent true;
      alphaTestRef 0.7;
	texture "texture/effects/e_flagpole2";
}

subshader "shield_down_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.611765 0.592157 0.996079;
	transparent true;
      alphaTestRef 0.7;
	texture "texture/effects/e_flagpole3";
}

