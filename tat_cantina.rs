subshader "tat_cantina_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/tatooine/tat_wall_stuco_door";
}

subshader "tat_cantina_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/tatooine/tat_wall_stuco_door";
}

subshader "tat_cantina_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/tatooine/tat_wall_stuco_door";
}

subshader "tat_cantina_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/tatooine/tat_wall_lstuco_metal";
}

subshader "tat_cantina_Material4" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/tatooine/tat_wall_lstuco_metal";
}

subshader "tat_cantina_Material5" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/tatooine/tat_wall_color";
}

subshader "tat_cantina_Material6" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/tatooine/tat_wall_stuco_door";
}

subshader "tat_cantina_Material7" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/tatooine/tat_wall_lstuco_metal";
}

subshader "tat_cantina_Material8" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/tatooine/tat_wall_stuco_door";
}

subshader "tat_cantina_Material9" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/tatooine/tat_bar_bright";
}

subshader "tat_cantina_Material10" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/tatooine/tat_floor_tile_small";
}

