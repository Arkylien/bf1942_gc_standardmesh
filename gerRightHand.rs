subshader "JapRightHand_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.1 0.1 0.1;
	materialSpecular 0.1 0.1 0.1;
	envmap true;
	texture "texture/snowtrooper/righthand.dds";
}
