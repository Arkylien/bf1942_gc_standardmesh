subshader "Sky_Redemption_M1_Material0" "StandardMesh/Default"
{
	lighting false;
	texture "bf1942/levels/GC_Redemption/Textures/Sky/Sky_Redemption_05";
}

subshader "Sky_Redemption_M1_Material1" "StandardMesh/Default"
{
	lighting false;
	texture "bf1942/levels/GC_Redemption/Textures/Sky/Sky_Redemption_06";
}

subshader "Sky_Redemption_M1_Material2" "StandardMesh/Default"
{
	lighting false;
	texture "bf1942/levels/GC_Redemption/Textures/Sky/Sky_Redemption_01";
}

subshader "Sky_Redemption_M1_Material3" "StandardMesh/Default"
{
	lighting false;
	texture "bf1942/levels/GC_Redemption/Textures/Sky/Sky_Redemption_02";
}

subshader "Sky_Redemption_M1_Material4" "StandardMesh/Default"
{
	lighting false;
	texture "bf1942/levels/GC_Redemption/Textures/Sky/Sky_Redemption_03";
}

subshader "Sky_Redemption_M1_Material5" "StandardMesh/Default"
{
	lighting false;
	texture "bf1942/levels/GC_Redemption/Textures/Sky/Sky_Redemption_04";
}

