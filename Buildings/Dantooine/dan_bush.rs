subshader "F_bush06_M1_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	transparent false;
	alphatestref 0.5;
	highEndPerPixel true;
	materialDiffuse .6 .6 .6;
	materialAmbient .8 .8 .8;
	selfillum .5 .5 .5;
	texture "texture/buildings/dantooine/dan_bush";
}

