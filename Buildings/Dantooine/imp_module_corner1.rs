subshader "imp_module_corner1_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/dantooine/imp_modules_id4";
}

subshader "imp_module_corner1_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.294118 0.305882 0.345098;
	texture "texture/buildings/dantooine/imp_modules_id5";
}

