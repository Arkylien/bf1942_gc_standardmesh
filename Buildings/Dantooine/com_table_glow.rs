subshader "com_table_glow_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
      transparent true;
	texture "texture/buildings/dantooine/hologram_table_id1";
}

subshader "com_table_glow_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
      transparent true;
	texture "texture/buildings/dantooine/hologram_table_id3";
}

