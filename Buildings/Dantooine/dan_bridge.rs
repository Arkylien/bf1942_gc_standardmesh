subshader "dan_bridge_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/buildings/dantooine/base";
}

subshader "dan_bridge_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/dantooine/rock_floor";
}

subshader "dan_bridge_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/buildings/dantooine/base02";
}

subshader "dan_bridge_Material3" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	texture "texture/buildings/dantooine/base01";
}

