subshader "imp_module_tower_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/dantooine/imp_modules_id2";
}

subshader "imp_module_tower_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	twosided true;
	texture "texture/buildings/dantooine/imp_modules_id3";
}

subshader "imp_module_tower_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/dantooine/imp_modules_id4";
}

subshader "imp_module_tower_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.294118 0.305882 0.345098;
      transparent true;
	texture "texture/buildings/dantooine/imp_modules_id5";
}

