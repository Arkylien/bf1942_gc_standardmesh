subshader "DSEntranceTest1_Material0" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\DS2\32DSENTRANCE_Tex00000";
}

subshader "DSEntranceTest1_Material1" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\DS2\32DSENTRANCE_Tex00001";
}

subshader "DSEntranceTest1_Material2" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\DS2\32DSENTRANCE_Tex00002";
}

subshader "DSEntranceTest1_Material3" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\DS2\32DSENTRANCE_Tex00003";
}

subshader "DSEntranceTest1_Material4" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\DS2\32DSENTRANCE_Tex00004";
}

