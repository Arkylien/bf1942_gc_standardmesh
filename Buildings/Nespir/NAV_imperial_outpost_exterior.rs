subshader "NAV_imperial_outpost_exterior_Material0" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/Nespir/ImpOutpostWalls1";
}

subshader "NAV_imperial_outpost_exterior_Material1" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/Nespir/ImpOutpostWallsPlain";
}

subshader "NAV_imperial_outpost_exterior_Material2" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/Nespir/ImpOutpostStairs";
}

subshader "NAV_imperial_outpost_exterior_Material3" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/Nespir/ImpOutpostFloorExt";
}

subshader "NAV_imperial_outpost_exterior_Material4" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/Nespir/ImpOutpostWalls2";
}

