subshader "imperial_hangar_Material0" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture\Buildings\Nespir\trench_hut";
}

subshader "imperial_hangar_Material1" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture\Buildings\Nespir\trench";
}

subshader "imperial_hangar_Material2" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture\Buildings\Nespir\imp_wall";
}

subshader "imperial_hangar_Material3" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture\Buildings\Nespir\Firehawk_pen";
}

subshader "imperial_hangar_Material4" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture\Buildings\Nespir\bridge";
}

