subshader "imperial_outpost_watchtower_Material0" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\Nespir\ImpOutpostWalls1";
}

subshader "imperial_outpost_watchtower_Material1" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\Nespir\ImpOutpostWallsPlain";
}

subshader "imperial_outpost_watchtower_Material2" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\Nespir\ImpOutpostWalls2";
}

subshader "imperial_outpost_watchtower_Material3" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\Nespir\ImpOutpostWalls3";
}

subshader "imperial_outpost_watchtower_Material4" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\Nespir\ImpOutpostFloorExt";
}

subshader "imperial_outpost_watchtower_Material5" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\Nespir\ImpOutpostIntFloor";
}

subshader "imperial_outpost_watchtower_Material6" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\Nespir\ImpOutpostIntWall1";
}

subshader "imperial_outpost_watchtower_Material7" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\Nespir\ImpOutpostIntWall2";
}

subshader "imperial_outpost_watchtower_Material8" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\Nespir\ImpOutpostIntWall3";
}

subshader "imperial_outpost_watchtower_Material9" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\Nespir\ImpOutpostIntLights";
}

subshader "imperial_outpost_watchtower_Material10" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\Nespir\ImpOutpostIntTile";
}

subshader "imperial_outpost_watchtower_Material11" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture\Buildings\Nespir\fixed\tan_computerterm";
}

