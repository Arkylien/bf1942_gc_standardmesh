subshader "echobase_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/Buildings/Hoth/Echobase/echobase_outside";
}
subshader "echobase_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/Buildings/Hoth/Echobase/echobase_hanger";
}
subshader "echobase_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/Buildings/Hoth/Echobase/notexture";
}
