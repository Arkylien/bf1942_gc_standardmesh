subshader "echo_contpan_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/SuperEcho/control_panel_A";
}

subshader "echo_contpan_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "mods/gcmod/movies/hud2.bik";
}

subshader "echo_contpan_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "mods/gcmod/movies/TieHud1_1.bik";
}

