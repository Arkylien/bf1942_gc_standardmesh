subshader "D_nebulonb_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/vehicles/nebulonb/tex1";
}

subshader "D_nebulonb_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/vehicles/nebulonb/tex7";
}

subshader "D_nebulonb_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/vehicles/nebulonb/tex6";
}

subshader "D_nebulonb_Material3" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/vehicles/nebulonb/tex5";
}

subshader "D_nebulonb_Material4" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/vehicles/nebulonb/tex4";
}

subshader "D_nebulonb_Material5" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/vehicles/nebulonb/tex3";
}

subshader "D_nebulonb_Material6" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/vehicles/nebulonb/tex2";
}

subshader "D_nebulonb_Material7" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/vehicles/nebulonb/rstripe";
}

subshader "D_nebulonb_Material8" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/buildings/tanaab/tan_isd01";
}

subshader "D_nebulonb_Material9" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/buildings/tanaab/tan_frig04";
}

subshader "D_nebulonb_Material10" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/buildings/tanaab/tan_isd04";
}

subshader "D_nebulonb_Material11" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/buildings/tanaab/tan_isd05";
}

