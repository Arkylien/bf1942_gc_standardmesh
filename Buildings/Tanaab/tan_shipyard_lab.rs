subshader "tan_shipyard_lab_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.592157 0.560784 1.0;
	texture "texture/Buildings/Tanaab/tan_shipyard01";
}

subshader "tan_shipyard_lab_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.0 0.996079 0.0;
	texture "texture/Buildings/Tanaab/tan_floorgrate";
}

subshader "tan_shipyard_lab_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 0.0 0.0;
	texture "texture/Buildings/Tanaab/tan_pipes";
}

subshader "tan_shipyard_lab_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.988235 1.0 0.0;
	texture "texture/Buildings/Tanaab/tan_wall01";
}

subshader "tan_shipyard_lab_Material4" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.964706 0.0 1.0;
	texture "texture/Buildings/Tanaab/tan_wall02";
}

subshader "tan_shipyard_lab_Material5" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.788235 0.67451 0.435294;
	texture "texture/Buildings/Tanaab/tan_wall03";
}

subshader "tan_shipyard_lab_Material6" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.4 0.564706 0.596078;
	texture "texture/Buildings/Tanaab/tan_pipes";
}

subshader "tan_shipyard_lab_Material7" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.850981 0.572549 0.572549;
	texture "texture/Buildings/Tanaab/tan_computerterm";
}

subshader "tan_shipyard_lab_Material8" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.0 0.329412 1.0;
	texture "texture/Buildings/Tanaab/tan_lights";
}

subshader "tan_shipyard_lab_Material9" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tanaab/tan_isd05";
}

