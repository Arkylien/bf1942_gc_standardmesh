subshader "tan_nebfrigate_hanger_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tanaab/tan_isd01";
}

subshader "tan_nebfrigate_hanger_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tanaab/tan_frig04";
}

subshader "tan_nebfrigate_hanger_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tanaab/tan_isd04";
}

subshader "tan_nebfrigate_hanger_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tanaab/tan_isd05";
}

