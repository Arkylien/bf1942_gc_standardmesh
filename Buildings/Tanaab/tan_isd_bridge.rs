subshader "tan_isd_bridge_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tanaab/tan_isd02";
}

subshader "tan_isd_bridge_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tanaab/tan_isd01";
}

