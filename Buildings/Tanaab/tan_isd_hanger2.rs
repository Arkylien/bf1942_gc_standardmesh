subshader "tan_isd_hanger2_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tanaab/tan_isd01";
}

subshader "tan_isd_hanger2_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tanaab/tan_isd03";
}

subshader "tan_isd_hanger2_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tanaab/tan_isd04";
}

subshader "tan_isd_hanger2_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tanaab/tan_isd05";
}

