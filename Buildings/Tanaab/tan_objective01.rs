subshader "tan_objective01_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.2 0.2 0.2;
	materialSpecularPower 4.0;
	texture "texture/Buildings/Tanaab/tan_shipyard01";
}

subshader "tan_objective01_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.2 0.2 0.2;
	materialSpecularPower 4.0;
	texture "texture/Buildings/Tanaab/tan_shipyard02";
}

subshader "tan_objective01_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.2 0.2 0.2;
	materialSpecularPower 4.0;
	texture "texture/Buildings/Tanaab/tan_shipyard03";
}

