subshader "yodahut_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/buildings/Endor/treebark_2";
}

subshader "yodahut_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/buildings/dagobah/yodahut";
}

