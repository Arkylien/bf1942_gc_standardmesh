subshader "jud_hall03_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	materialSpecular 0.1 0.1 0.1;
	envmap true;
	lightingSpecular true;
	texture "texture/buildings/judicator/jud_floor";
}

subshader "jud_hall03_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	materialSpecular 0.1 0.1 0.1;
	envmap true;
	lightingSpecular true;
	texture "texture/buildings/judicator/jud_wall02";
}

subshader "jud_hall03_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	materialSpecular 0.1 0.1 0.1;
	envmap true;
	lightingSpecular true;
	texture "texture/buildings/judicator/jud_floornospec";
}

subshader "jud_hall03_Material3" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	materialSpecular 0.1 0.1 0.1;
	envmap true;
	lightingSpecular true;
	texture "texture/buildings/judicator/jud_wall01";
}

