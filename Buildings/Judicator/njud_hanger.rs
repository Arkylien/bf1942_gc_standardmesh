subshader "njud_hanger_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	envmap true;
	lightingSpecular true;
        twosided true;
	texture "texture/buildings/judicator/jud_floor";
}

subshader "njud_hanger_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	envmap true;
	lightingSpecular true;
        twosided true;
	texture "texture/buildings/judicator/jud_wall02";
}

subshader "njud_hanger_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	envmap true;
	lightingSpecular true;
        twosided true;
	texture "texture/buildings/judicator/jud_floornospec";
}

subshader "njud_hanger_Material3" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	envmap true;
	lightingSpecular true;
	twosided true;
	texture "texture/buildings/judicator/jud_rivits";
}

subshader "njud_hanger_Material4" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	envmap true;
	lightingSpecular true;
        twosided true;
	texture "texture/buildings/judicator/jud_underlight";
}

