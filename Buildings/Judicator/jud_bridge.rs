subshader "Material #2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	materialSpecular 0.1 0.1 0.1;
	envmap true;
	lightingSpecular true;
	texture "texture/buildings/judicator/jud_floor";
}

subshader "jud_bridge_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	materialSpecular 0.1 0.1 0.1;
	envmap true;
	lightingSpecular true;
	texture "texture/buildings/judicator/jud_wall02";
}

subshader "jud_bridge_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	materialSpecular 0.1 0.1 0.1;
	envmap true;
	lightingSpecular true;
	texture "texture/buildings/judicator/jud_wall01";
}

subshader "jud_bridge_Material3" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	materialSpecular 0.1 0.1 0.1;
	envmap true;
	lightingSpecular true;
	texture "texture/buildings/judicator/jud_frontpanel";
}

subshader "jud_bridge_Material4" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	materialSpecular 0.1 0.1 0.1;
	envmap true;
	lightingSpecular true;
	texture "texture/buildings/judicator/jud_pipes";
}

subshader "jud_bridge_Material5" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	materialSpecular 0.1 0.1 0.1;
	envmap true;
	lightingSpecular true;
	texture "texture/buildings/judicator/jud_wall01";
}

subshader "Material #11" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	materialSpecular 0.5 0.5 0.5;
	envmap false;
	lightingSpecular false;
	transparent true;
	texture "texture/buildings/judicator/jud_glass";
}

