subshader "2 - Default" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 1.0 1.0 1.0;
	materialSpecularPower 10.0;
	twosided true;
	envmap true;
	texture "texture/buildings/judicator/jud_armory";
}

