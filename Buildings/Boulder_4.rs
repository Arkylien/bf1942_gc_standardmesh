subshader "Boulder_4_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/boulder_4_1";
}
subshader "Boulder_4_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/boulder_4_3";
}
subshader "Boulder_4_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/boulder_4_5";
}
subshader "Boulder_4_Material3" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/boulder_4_2";
}
subshader "Boulder_4_Material4" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/boulder_4_4";
}
