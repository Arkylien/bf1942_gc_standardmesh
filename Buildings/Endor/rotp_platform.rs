subshader "rotp_platform_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Buildings/Endor/rotp_platform";
}

subshader "rotp_platform_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Buildings/Endor/jud_floornospec";
}

