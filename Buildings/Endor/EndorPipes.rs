subshader "EndorPipes_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Endor/bunker_ceiling";
}

subshader "EndorPipes_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Endor/Bunker_walls";
}

