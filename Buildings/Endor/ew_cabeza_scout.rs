subshader "ew_cabeza_scout_Material0" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/stormt_helmet_redone_uvw_v2";
}

subshader "ew_cabeza_scout_Material1" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/Buildings/Dantooine/dant_rock";
}

subshader "ew_cabeza_scout_Material2" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/Buildings/Endor/longbridge";
}

subshader "ew_cabeza_scout_Material3" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	twosided true;
	texture "texture/Buildings/Endor/plumas3";
}

