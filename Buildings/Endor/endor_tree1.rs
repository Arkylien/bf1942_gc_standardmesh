subshader "endor_tree1_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	twosided true;
	texture "texture/buildings/endor/treebark";
}

subshader "endor_tree1_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
      twosided true;
	texture "texture/buildings/endor/leaf1";
}

