subshader "curve_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/Endor/curvetexleft";
}
subshader "curve_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/Endor/curvetexmid";
}
subshader "curve_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/Endor/curvetexright";
}
