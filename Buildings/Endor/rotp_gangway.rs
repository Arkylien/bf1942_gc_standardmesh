subshader "rotp_gangway_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Buildings/Endor/jud_floornospec";
}

subshader "rotp_gangway_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Buildings/Endor/bunker_floor";
}

subshader "rotp_gangway_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Buildings/Endor/jud_floornospec";
}

