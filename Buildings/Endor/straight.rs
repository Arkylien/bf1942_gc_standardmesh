subshader "straight_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/Endor/straight2";
}
subshader "straight_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/Endor/straight1";
}
subshader "straight_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/Endor/straight3";
}
