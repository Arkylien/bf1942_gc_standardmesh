subshader "rotp_beam_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.5 0.5 0.5;
	materialSpecular 0.9 0.9 0.9;
	materialSpecularPower 5.0;
	texture "texture/Buildings/Endor/rotp_beam";
}

