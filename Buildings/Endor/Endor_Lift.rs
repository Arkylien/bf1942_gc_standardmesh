subshader "Endor_Lift_Material0" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/buildings/endor/rotp_platform";
}

subshader "Endor_Lift_Material1" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/buildings/judicator/jud_door01";
}

