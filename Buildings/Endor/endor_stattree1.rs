subshader "endor_stattree1_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	twosided true;
	texture "texture/buildings/endor/Spruse_T";
}

subshader "endor_stattree1_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/endor/treebark";
}

