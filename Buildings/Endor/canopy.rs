subshader "canopy_Material0" "StandardMesh/Default"
{
	lightingSpecular false;
	transparent true;
	blendSrc sourceAlpha; 
	twosided true;
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/canopy";
}

