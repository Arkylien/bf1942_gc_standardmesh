subshader "endor_fern1_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.6 0.6 0.6;
	twosided true;
	alphaTestRef 0.5;
	highendperpixel true;
	materialambient .8 .8 .8;
	selfillum .4 .4 .4;
	texture "texture/Buildings/Endor/endor_fern1";
}

