subshader "rotp_column_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Buildings/Endor/jud_floornospec";
}

subshader "rotp_column_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Buildings/Endor/bunker_floor";
}

subshader "rotp_column_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Buildings/Endor/bunker_lights";
}

subshader "rotp_column_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/Buildings/Judicator/jud_wall02";
}

