subshader "riserpiece_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/Endor/risetexmid";
}
subshader "riserpiece_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/Endor/risetexright";
}
subshader "riserpiece_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	lightingSpecular false;
	texture "texture/Buildings/Endor/risetexleft";
}
