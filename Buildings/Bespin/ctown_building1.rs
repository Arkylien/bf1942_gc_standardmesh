subshader "ctown_building1_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1.0 1.0 0.1.0;
	lightingSpecular false;
	materialSpecular 0.1 0.1 0.1;
	materialSpecularPower 1.0;
	envmap true;
	texture "texture/Buildings/Bespin/ctown_building_id1";
	normalmap "texture/Buildings/Bespin/ctown_building_id1_bump";
}
subshader "ctown_building1_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1.0 1.0 0.1.0;
	lightingSpecular false;
	materialSpecular 0.1 0.1 0.1;
	materialSpecularPower 1.0;
	envmap true;
	texture "texture/Buildings/Bespin/ctown_building_id7";
}
subshader "ctown_building1_Material2" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 1.0 1.0 0.1.0;
	lightingSpecular false;
	materialSpecular 0.1 0.1 0.1;
	materialSpecularPower 1.0;
	envmap true;
	texture "texture/Buildings/Bespin/ctown_building_id4";
}
subshader "ctown_building1_Material3" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1.0 1.0 0.1.0;
	lightingSpecular false;
	materialSpecular 0.1 0.1 0.1;
	materialSpecularPower 1.0;
	envmap true;
	texture "texture/Buildings/Bespin/ctown_walkway_id8";
}
