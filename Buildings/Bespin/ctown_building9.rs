subshader "ctown_building9_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 4.0;
	envmap true;
	texture "texture/Buildings/Bespin/ctown_building_id3";
}
subshader "ctown_building9_Material1" "StandardMesh/Default"
{
	lighting false;
	materialDiffuse 0.588235 0.588235 0.588235;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 4.0;
	envmap false;
	texture "texture/Buildings/Bespin/ctown_building_id6";
}
subshader "ctown_building9_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	lightingSpecular true;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 4.0;
	envmap true;
	texture "texture/Buildings/Bespin/ctown_walkway_id8";
}
