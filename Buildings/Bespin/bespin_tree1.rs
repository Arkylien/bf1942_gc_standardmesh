subshader "bespin_tree1_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.8 0.8 0.8;
	materialSpecularPower 1.0;
	twosided true;
        transparent true;
	texture "texture/Buildings/Bespin/bespin_tree1";
}