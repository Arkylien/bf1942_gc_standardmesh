subshader "bespin_monument2_Material0" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
        twosided true;
	texture "texture/Buildings/Bespin/ctown_building_id1";
}

subshader "bespin_monument2_Material1" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
        transparent true;
        depthWrite false;
	materialDiffuse 1.0 1.0 1.0;
	twosided true;
	texture "texture/Buildings/Bespin/bespin_monument2_id3";
}

subshader "bespin_monument2_Material2" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
        twosided true;
	texture "texture/Buildings/Bespin/ctown_building_id12";
}

