subshader "mc_dock_outside_Material0" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/moncal/mc_hull";
}

subshader "mc_dock_outside_Material1" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/moncal/mc_ramp";
}

subshader "mc_dock_outside_Material2" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/moncal/mc_hull3";
}

subshader "mc_dock_outside_Material3" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.945098 0.760785 0.596078;
	texture "texture/buildings/moncal/mc_door_ring";
}

subshader "mc_dock_outside_Material4" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/moncal/mc_door_inside";
}

subshader "mc_dock_outside_Material5" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/moncal/mc_hull2";
}

subshader "mc_dock_outside_Material6" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/moncal/mc_hull4";
}

