subshader "lander_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.501961 0.501961 0.501961;
	texture "texture/buildings/moncal/landing_craft1";
}

subshader "lander_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.501961 0.501961 0.501961;
	texture "texture/buildings/moncal/landing_craft_ceil";
}

subshader "lander_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.584314 0.584314 0.584314;
	texture "texture/buildings/moncal/landing_craft_wall";
}

subshader "lander_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.584314 0.584314 0.584314;
	texture "texture/buildings/moncal/landing_craft_flr";
}

subshader "lander_Material4" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.584314 0.584314 0.584314;
	texture "texture/buildings/moncal/landing_craft_ceil";
}

subshader "lander_Material5" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.584314 0.584314 0.584314;
	texture "texture/buildings/moncal/landing_craft1";
}

subshader "lander_Material6" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.584314 0.584314 0.584314;
	transparent true;
	texture "texture/buildings/moncal/landing_craft1";
}

subshader "lander_Material7" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.133333 0.133333 0.133333;
	transparent true;
	texture "texture/buildings/moncal/notexture";
}

