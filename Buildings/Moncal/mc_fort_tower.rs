subshader "mc_fort_tower_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/moncal/mc_fort_wall3";
}

subshader "mc_fort_tower_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/moncal/mc_fort_wall2";
}

subshader "mc_fort_tower_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/superecho/gmappart2";
}

subshader "mc_fort_tower_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	twosided true;
	texture "texture/buildings/tanaab/tan_floorgrate";
}

