subshader "D_rebtrans_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/buildings/hoth/EngineGlow_ID_3";
}

subshader "D_rebtrans_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/buildings/hoth/hull_ID_1";
}

subshader "D_rebtrans_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/buildings/hoth/cargo_ID_2";
}

subshader "D_rebtrans_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/buildings/hoth/glow_ID_4";
}

