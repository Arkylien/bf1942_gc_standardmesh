subshader "pod_tracklight_Material0" "StandardMesh/Default"
{
	materialSpecular 0.8 0.8 1.0; 
	materialSpecularPower 4.0;
	lighting true;
	lightingSpecular true;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/Buildings/Tatooine/pod_tracklight";
}

