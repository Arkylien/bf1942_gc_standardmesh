subshader "tat_medium_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.298039 0.164706 0.964706;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}

subshader "tat_medium_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.929412 0.4 0.94902;
	texture "texture/Buildings/Tatooine/tat_wall_lstuco_metal";
}

subshader "tat_medium_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.0980392 0.945098 0.396079;
	texture "texture/Buildings/Tatooine/tat_floor_tile_small";
}

subshader "tat_medium_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.635294 0.615686 0.568627;
	texture "texture/Buildings/Tatooine/tat_wall_light";
}

subshader "tat_medium_Material4" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.298039 0.164706 0.964706;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}

