subshader "tat_launchbay_inside01_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.811765 0.521569 0.521569;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}

subshader "tat_launchbay_inside01_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.682353 0.592157 0.984314;
	texture "texture/Buildings/Tatooine/tat_floor_tile_small";
}

subshader "tat_launchbay_inside01_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.811765 0.521569 0.521569;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}

