subshader "pod_stadiumseats_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/buildings/Tatooine/tat_wall_light";
}

subshader "pod_stadiumseats_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/buildings/Tatooine/tat_floor_tile_medium";
}

