subshader "innerstarport_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/Buildings/Tatooine/tat_wall_color";
}

subshader "innerstarport_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.952941 0.811765 0.592157;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}

subshader "innerstarport_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.952941 0.811765 0.592157;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}

subshader "innerstarport_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.952941 0.811765 0.592157;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}

subshader "innerstarport_Material4" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
	texture "texture/Buildings/Tatooine/tat_wall_lstuco_metal";
}

subshader "innerstarport_Material5" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.952941 0.811765 0.592157;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}

