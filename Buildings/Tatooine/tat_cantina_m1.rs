subshader "tat_cantina_m1_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/buildings/Tatooine/tat_wall_color";
}
subshader "tat_cantina_m1_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/buildings/Tatooine/tat_floor_tile_small";
}
subshader "tat_cantina_m1_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/buildings/Tatooine/tat_wall_stuco_door";
}
subshader "tat_cantina_m1_Material3" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/buildings/Tatooine/tat_wall_lstuco_metal";
}
subshader "tat_cantina_m1_Material4" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/buildings/Tatooine/tat_wall_stuco_door";
}
