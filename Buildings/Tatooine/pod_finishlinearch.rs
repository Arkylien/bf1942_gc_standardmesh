subshader "pod_finishlinearch_Material0" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/Tatooine/tat_wall_light";
}

subshader "pod_finishlinearch_Material1" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/Tatooine/tat_floor_tile_medium";
}

