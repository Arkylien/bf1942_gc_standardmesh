subshader "tat_sarlacc_Material0" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/buildings/tatooine/tat_sand";
}

subshader "tat_sarlacc_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/tatooine/tat_bar_bright";
}

subshader "tat_sarlacc_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/tatooine/tat_sarlacc";
}

