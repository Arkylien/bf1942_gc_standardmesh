subshader "pod_finishlinetower_Material0" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/Tatooine/tat_wall_light";
}

subshader "pod_finishlinetower_Material1" "StandardMesh/Default"
{
	lighting false;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/buildings/Tatooine/tat_floor_tile_medium";
}

