subshader "Cantina_m2_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}
subshader "Cantina_m2_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}
subshader "Cantina_m2_Material2" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588235 0.588235 0.588235;
	lightingSpecular false;
	texture "texture/black_o";
}
subshader "Cantina_m2_Material3" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/Buildings/Tatooine/tat_wall_lstuco_metal";
}
subshader "Cantina_m2_Material4" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/Buildings/Tatooine/tat_wall_lstuco_metal";
}
subshader "Cantina_m2_Material5" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/Buildings/Tatooine/tat_wall_color";
}
subshader "Cantina_m2_Material6" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}
subshader "Cantina_m2_Material7" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/Buildings/Tatooine/tat_floor_tile_small";
}
