subshader "tat_ashtray_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 1 1 1;
	materialSpecular 0.1 0.1 0.1;
	envmap true;
	lightingSpecular true;
	transparent true;
	texture "texture/Buildings/Tatooine/ashtray";
}

