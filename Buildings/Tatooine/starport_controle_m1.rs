subshader "starport_controle_m1_Material0" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/buildings/Tatooine/tat_wall_stuco_door";
}
subshader "starport_controle_m1_Material1" "StandardMesh/Default"
{
	lighting true;
	materialDiffuse 0.588 0.588 0.588;
	lightingSpecular false;
	texture "texture/buildings/Tatooine/tat_wall_lstuco_metal";
}
