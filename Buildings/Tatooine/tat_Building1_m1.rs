subshader "tat_Building1_m1_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tatooine/tat_stucco1";
}

subshader "tat_Building1_m1_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tatooine/tat_wood_1";
}

