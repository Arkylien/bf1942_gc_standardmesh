subshader "tat_small_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}

subshader "tat_small_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.592157 0.631373 0.843137;
	texture "texture/Buildings/Tatooine/tat_wall_lstuco_metal";
}

subshader "tat_small_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.603922 0.588235 0.917647;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}

subshader "tat_small_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}

subshader "tat_small_Material4" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}

subshader "tat_small_Material5" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588 0.588 0.588;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}

