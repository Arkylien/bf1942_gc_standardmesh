subshader "starport_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.952941 0.811765 0.592157;
	texture "texture/buildings/tatooine/tat_wall_stuco_door";
}

subshader "Material #6" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.952941 0.811765 0.592157;
	texture "texture/buildings/tatooine/tat_wall_stuco_door";
}

subshader "Material #7" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.972549 0.941177 0.843137;
	texture "texture/buildings/tatooine/tat_wall_lstuco_metal";
}

subshader "Material #4" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.972549 0.941177 0.843137;
	texture "texture/buildings/tatooine/tat_wall_lstuco_metal";
}

