subshader "RebPilot_helm_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	texture "texture/rebelpilot_helmet_tex";
}

subshader "RebPilot_helm_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 1.0 1.0 1.0;
	envmap true;
	texture "Mods\GCMOD\Movies\rebeleyes";
}
