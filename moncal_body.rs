subshader "moncal_body_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.454902 0.454902 0.415686;
	texture "texture/moncal_body_id1";
}

subshader "moncal_body_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.486275 0.254902 0.0705882;
	texture "texture/moncal_body_id2";
}

subshader "moncal_body_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.803922 0.803922 0.803922;
	texture "texture/moncal_body_id3";
}

