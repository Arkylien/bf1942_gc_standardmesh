subshader "spacetorp_m1_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular true;
	materialDiffuse 1.0 1.0 1.0;
	materialSpecular 0.623529 0.623529 0.623529;
	materialSpecularPower 8.5;
	texture "texture/Torpedo_base_Z";
}

