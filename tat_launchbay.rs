subshader "tat_launchbay_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.811765 0.521569 0.521569;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}

subshader "tat_launchbay_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.643137 0.643137 0.643137;
	texture "texture/Buildings/Tatooine/tat_wall_lstuco_metal";
}

subshader "tat_launchbay_Material2" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.937255 0.866667 0.0901961;
	texture "texture/Buildings/Tatooine/tat_wall_light";
}

subshader "tat_launchbay_Material3" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.811765 0.521569 0.521569;
	twosided true;
	texture "texture/Buildings/Tatooine/tat_wall_stuco_door";
}

