subshader "rebel_scout_shaw_Material0" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
      twosided true;
	texture "texture/scout_shawl";
}

subshader "rebel_scout_shaw_Material1" "StandardMesh/Default"
{
	lighting true;
	lightingSpecular false;
	materialDiffuse 0.588235 0.588235 0.588235;
      transparent true;
	texture "texture/scout_headset";
}

